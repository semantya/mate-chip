# Neuro :: Chip

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

Python Social Auth aims to be an easy to setup social authentication and authorization mechanism for Python projects supporting protocols like OAuth (1 and 2), OpenId and others.

The initial codebase is derived from django-social-auth with the idea of generalizing the process to suit the different frameworks around, providing the needed tools to bring support to new frameworks.

django-social-auth itself was a product of modified code from django-twitter-oauth and django-openid-auth projects.

NeuroChip is a Django application using PSA with RedisQueue support.

## List of Adapters

* IRC
* HipChat
* Facebook Messenger
* Slack
* Telegram


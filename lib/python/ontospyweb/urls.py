from django.conf.urls import *

urlpatterns = patterns('ontospyweb.views',
	url(r'^ontology$',      'ontology',     name='ontology'),
	url(r'^classes$',       'classes',      name='classes'),
	url(r'^properties$',    'properties',   name='properties'),
    url(r'^individuals$',   'individuals',  name='individuals'),

	url(r'^$',              'ontoDocsMain', name='ontodocs_main'),
)


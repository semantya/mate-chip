from gestalt.web.helpers import *

DjConfig = yaml.load(open(Reactor.SPECS('config','django.yml')))

################################################################################

GHOST_FQDN = "%(GHOST_PERSON)s.%(GHOST_DOMAIN)s" % os.environ

META_SITE_PROTOCOL = DjConfig['routing'].get('scheme', 'https')
META_SITE_DOMAIN   = DjConfig['routing'].get('domain', GHOST_FQDN)

TIME_ZONE     = DjConfig['i18n'].get('timezone',  'Africa/Casablanca')
COUNTRY_CODE  = DjConfig['i18n'].get('country', 'us')
DIALECT_CODE  = DjConfig['i18n'].get('language', 'en')

LANGUAGE_CODE = '%s-%s' % (DIALECT_CODE, COUNTRY_CODE)

#*******************************************************************************

ADMINS = (
    # ('Your Name', 'your_email@uchikoma.com'),
)

KIMINO_THEMES = os.environ.get('DJANGO_THEMES', 'gentelella').split(' ')

################################################################################

ROOT_URLCONF = 'uchikoma.landing.urls.catcher'

WSGI_APPLICATION = 'gestalt.web.wsgi.application'

SUBDOMAIN_URLCONFS = {
    None:         'uchikoma.landing.urls.catcher',

    #"apis":     UCHI_URLS('xxxxx','xxxxx'),
    'staging':    'gestalt.web.urls.Staging',

    'backoffice': 'gestalt.web.urls.Honey',
    'manager':    'gestalt.web.urls.Honey',
    'admin':      'gestalt.web.urls.Honey',

    #"call":     UCHI_URLS('xxxxx','xxxxx'),
    #"discuss":  UCHI_URLS('xxxxx','xxxxx'),
    #"talk":     UCHI_URLS('xxxxx','xxxxx'),

    #"support":  UCHI_URLS('xxxxx','xxxxx'),
    #"reflect":  UCHI_URLS('xxxxx','xxxxx'),
    #"reason":   UCHI_URLS('xxxxx','xxxxx'),
    #"web":      UCHI_URLS('xxxxx','xxxxx'),
    #"platform": UCHI_URLS('xxxxx','xxxxx'),
    #"metrics":  UCHI_URLS('xxxxx','xxxxx'),
    #"swarm":    UCHI_URLS('xxxxx','xxxxx'),
}

for key in Reactor.router.subdomains:
    SUBDOMAIN_URLCONFS[key] = Reactor.router.subdomains[key].ns_urls

################################################################################

SUIT_CONFIG = {
    'ADMIN_NAME': 'Uchikoma Suit',
    'MENU': (
        {'label': 'Security', 'icon':'icon-lock', 'models': [
            {'label': 'HoneyPot Attempts',     'model': 'admin_honeypot.loginattempt'},
            {'label': 'Report formats',        'model': 'report_builder.format'},
            {'label': 'Report listing',        'model': 'report_builder.report'},
        ]},
        {'label': 'Authorization', 'icon':'icon-key', 'models': [
            {'label': 'Sites',                 'model': 'sites.site'},
            {'label': 'API keys',              'model': 'tastypie.api_key'},
            {'label': 'Oauth2 Access Tokens',  'model': 'provider.accesstoken'},
            {'label': 'Oauth2 Clients',        'model': 'provider.client'},
            {'label': 'Oauth2 Grants',         'model': 'provider.grant'},
            {'label': 'Oauth2 Refresh Tokens', 'model': 'provider.refreshtoken'},
            {'label': 'Social Associations',   'model': 'social.association'},
            {'label': 'Social Nonces',         'model': 'social.nonce'},
            {'label': 'Social User Auth',      'model': 'social.usersocialauth'},
        ]},
        '-',
        {'label': 'Connector', 'icon':'icon-plug', 'models': [
            {'label': 'Groups',                'model': 'auth.group'},
            {'label': 'Identities',            'model': 'connector.identity'},
            {'label': 'Organizations',         'model': 'connector.organization'},
        ]},
        {'label': 'Console', 'icon':'icon-desktop', 'models': [
            {'label': 'Projects',              'model': 'console.project'},
            {'label': 'Teams',                 'model': 'console.team'},
            {'label': 'Queries',               'model': 'console.query'},
        ]},
        '-',
        'cables',
        '-',
        'semantics',
        'opendata',
        '-',
        {'label': 'Redis-Queue',    'url': '/queue/',   'icon':'icon-cogs'},
        {'label': 'SQL Explorer',   'url': '/sql/',     'icon':'icon-database'},
        '-',
        {'label': 'Mongo Naut',     'url': '/mongo/',   'icon':'icon-server'},
        {'label': 'Report Builder', 'url': '/report/',  'icon':'icon-bug'},
    ),
    'HEADER_DATE_FORMAT': 'l, j. F Y', # Saturday, 16th March 2013
    'HEADER_TIME_FORMAT': 'H:i',       # 18:42
    'CONFIRM_UNSAVED_CHANGES': True,
}

################################################################################

INSTALLED_APPS = (
    'suit',
    'whitenoise.runserver_nostatic',

    'django.contrib.auth',

    #'neo4django.admin',
    #'neo4django.contenttypes',
    'django.contrib.admin',
    'django.contrib.contenttypes',

    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'bootstrap_toolkit',
    'sekizai',

) + tuple([x for x in DjConfig['apps'].get('builtins', [])]) + (

    'django_extensions',
    'django_rq',
    'django_mongoengine',
    'social.apps.django_app.default',

    'subdomains',
    'corsheaders',
    'meta',

    'provider',
    'provider.oauth2',

    #'axes',
    'rules',

    'tastypie',
    #'tastypie_mongoengine',

) + tuple([x for x in DjConfig['apps'].get('common', [])]) + (

    'django_countries',
    #'location_field.apps.DefaultConfig',

) + tuple([x for x in DjConfig['apps'].get('3rd-party', [])]) + (

    'suit_rq',
    'mongonaut',
    'admin_honeypot',

    #'ontospyweb',

) + tuple([x for x in DjConfig['apps'].get('contrib', [])])

################################################################################

CACHES = {
    'local': {
       'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
       'LOCATION': 'localhost:11211',
    }
}

for var_key,var_env in {
    'local': 'REDISCLOUD_URL',
    #'cloud': 'REDISCLOUD_URL',
}.iteritems():
    if var_env in Reactor.environ:
        link = Reactor.varlink(var_env)

        CACHES[var_key] = {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': '%s:%s' % (link.hostname, link.port),
            'OPTIONS': {
                'PASSWORD': link.password,
                'DB': 0,
            },
        }

#*******************************************************************************

DATABASES = {
    'local': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': Reactor.FILES('main.sqlite'),
    },
}

if 'DATABASE_URL' in Reactor.environ:
    link = Reactor.varlink('DATABASE_URL')

    DATABASES['heroku'] = {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        "NAME":     link.path[1:],
        "HOST":     link.hostname,
        "PORT":     link.port,
        "PASSWORD": link.password,
        "USER":     link.username,
    }

if 'CLEARDB_DATABASE_URL' in Reactor.environ:
    link = Reactor.varlink('CLEARDB_DATABASE_URL')

    DATABASES['cleardb'] = {
        'ENGINE':   'django.db.backends.mysql',
        "NAME":     link.path[1:],
        "HOST":     link.hostname,
        "PORT":     link.port,
        "PASSWORD": link.password,
        "USER":     link.username,
    }

#*******************************************************************************

NEO4J_DATABASES = {
    'local' : {
        'HOST':'localhost',
        'PORT':7474,
        'ENDPOINT':'/db/data'
    },
    'scubadev': {
        "ENDPOINT": '/db/data',
        "HOST":     'scubadev.sb05.stations.graphenedb.com',
        "PORT":     24789,
        "USERNAME": 'scuba_dev',
        "PASSWORD": 'zdZ4ejSxxgMMtRdSwhcT',
    },
    'tayaa': {
        "ENDPOINT": '/db/data',
        "HOST":     'tayaa.sb10.stations.graphenedb.com',
        "PORT":     24789,
        "USERNAME": 'tayaa',
        "PASSWORD": '58Ntq9bOrS9L5XRILroU',
    },
}

if 'GRAPHENEDB_URL' in Reactor.environ:
    link = Reactor.varlink('GRAPHENEDB_URL')

    NEO4J_DATABASES['graphene'] = {
        "ENDPOINT": link.path,
        "HOST":     link.hostname,
        "PORT":     link.port,
        "PASSWORD": link.password,
        "USERNAME": link.username,
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    }

#*******************************************************************************

MONGODB_DATABASES = {
    "local": {
        "name":     'uchikoma',
        "host":     'localhost',
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    },
}

if 'MONGODB_URI' in Reactor.environ:
    link = Reactor.varlink('MONGODB_URI')

    MONGODB_DATABASES['mongolab'] = {
        "name":     link.path[1:],
        "host":     link.hostname,
        "port":     link.port,
        "password": link.password,
        "username": link.username,
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    }

#*******************************************************************************

def get_rdf_links(**mapping):
    resp = {}

    if 'FUSEKI_URI' in Reactor.environ:
        yield 'local', Reactor.varlink('FUSEKI_URI')

    for source,alias in [
        ('mokatat',   'arts'),
        ('shivha',    'business'),
        ('inbijas',   'cables'),
        ('scubadev',  'coding'),
        ('itissal',   'common'),
        ('scubaops',  'devops'),
        ('hack2use',  'internet'),
        ('samurai',   'literature'),
        ('shinobi',   'metrics'),
        ('tayaa',     'science'),
        ('eclectic',  'semantics'),
        ('amanohoko', 'esoteric'),
        #('xxxxx',     'entertain'),
    ]:
        yield alias, urlparse('http://knowledge-%s.rhcloud.com/' % source)

RDF_DATABASES = {}

for name,link in get_rdf_links(local='FUSEKI_URL'):
    RDF_DATABASES[name] = {
        "path":     link.path[1:],
        "host":     link.hostname,
        "port":     link.port,
        "password": link.password,
        "username": link.username,
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)
    }

################################################################################

CACHES['default']            = CACHES.get('cloud',               CACHES['local'])
DATABASES['default']         = DATABASES.get('heroku',           DATABASES['local'])
#DATABASES['default']         = DATABASES.get('heroku', DATABASES.get('cleardb', DATABASES['local'])

NEO4J_DATABASES['default']   = NEO4J_DATABASES.get('graphene',   NEO4J_DATABASES['local'])
MONGODB_DATABASES['default'] = MONGODB_DATABASES.get('mongolab', MONGODB_DATABASES['local'])

#DATABASE_ROUTERS = ['neo4django.utils.Neo4djangoIntegrationRouter']

################################################################################

LOGIN_URL = '//apis.%s/login/' % META_SITE_DOMAIN
LOGIN_REDIRECT_URL = '//apis.%s/' % META_SITE_DOMAIN
URL_PATH = ''

STATIC_HOST = 'http://cdn.%s' % META_SITE_DOMAIN

#*******************************************************************************

MEDIA_ROOT = Reactor.FILES('media')
MEDIA_URL = '/media/'

#*******************************************************************************

STATIC_ROOT = Reactor.FILES('static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATICFILES_DIRS = [
    Reactor.FILES('assets'),
    #Reactor.FILES('cdn'),
]+[
    Reactor.THEMES(skin,'assets')
    for skin in KIMINO_THEMES
]

#*******************************************************************************

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

WHITENOISE_ROOT = Reactor.FILES()

WHITENOISE_AUTOREFRESH = True

WHITENOISE_MIMETYPES = {

}

WHITENOISE_SKIP_COMPRESS_EXTENSIONS = (
    'jpg', 'jpeg', 'png', 'gif', 'webp',
    'zip', 'gz', 'tgz', 'bz2', 'tbz',
    'swf', 'flv', 'woff',
)

################################################################################

DEVSERVER_MODULES = (
    'devserver.modules.sql.SQLRealTimeModule',
    'devserver.modules.sql.SQLSummaryModule',
    'devserver.modules.profile.ProfileSummaryModule',

    'devserver.modules.ajax.AjaxDumpModule',
    #'devserver.modules.profile.MemoryUseModule',
    'devserver.modules.cache.CacheSummaryModule',
    'devserver.modules.profile.LineProfilerModule',
)

DEVSERVER_IGNORED_PREFIXES = []

################################################################################

CRISPY_TEMPLATE_PACK = 'bootstrap'

TEMPLATE_TAG_BUILTINS = {
    'i18n':               'django.templatetags.i18n',
    'staticfiles':        'django.contrib.staticfiles.templatetags.staticfiles',

    #'sekizai_tags':      '',
    #'bootstrap_toolkit': '',

    'subdomainurls':     'subdomains.templatetags.subdomainurls',
    'backend_utils':     'uchikoma.connector.templatetags.backend_utils',
    'uchikoma':          'uchikoma.connector.templatetags.uchikoma',
}

#*******************************************************************************

TEMPLATE_EXTRA_PROCESSORS = (
    'social.apps.django_app.context_processors.backends',
    'sekizai.context_processors.sekizai',
)

#*******************************************************************************

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'APP_DIRS': True,
    'DIRS': [
        Reactor.FILES('templates'),
    ]+[
        Reactor.THEMES(skin,'templates')
        for skin in KIMINO_THEMES
    ],
    'OPTIONS': {
        'context_processors': (
            'django.template.context_processors.debug',
            'django.template.context_processors.request',

            "django.contrib.auth.context_processors.auth",
            "django.template.context_processors.csrf",

            "django.template.context_processors.i18n",
            "django.template.context_processors.media",
            "django.template.context_processors.static",
            "django.template.context_processors.tz",

            "django.contrib.messages.context_processors.messages",

            'gestalt.web.middlewares.Enrich',
        ) + TEMPLATE_EXTRA_PROCESSORS,
        #'libraries': TEMPLATE_TAG_BUILTINS,
    }
}]

TEMPLATE_DIRS = [
    pth
    for cfg in TEMPLATES
    for pth in cfg.get('DIRS', [])
]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
) + TEMPLATE_EXTRA_PROCESSORS

#*******************************************************************************

MIDDLEWARE_CLASSES = (
    'gestalt.web.middlewares.Startup',
    #'django.middleware.security.SecurityMiddleware',

    'whitenoise.middleware.WhiteNoiseMiddleware',
    'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'devserver.middleware.DevServerMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',

    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

################################################################################

from gestalt.web.credentials import *

SECRET_KEY = Reactor.varenv('SESSION_SECRET', '#$5btppqih8=%ae^#&amp;7en#kyi!vh%he9rg=ed#hm6fnw9^=umc')

#*******************************************************************************

SESSION_ENGINE = 'django_mongoengine.sessions'
SESSION_SERIALIZER = 'django_mongoengine.sessions.BSONSerializer'

#*******************************************************************************

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

################################################################################

AUTH_USER_MODEL = 'connector.Identity'

DEFAULT_AUTH_BACKENDS = (
    'social.backends.email.EmailAuth',
    'social.backends.username.UsernameAuth',

    #'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

#*******************************************************************************

SOCIAL_AUTH_STRATEGY = 'social.strategies.django_strategy.DjangoStrategy'
SOCIAL_AUTH_STORAGE = 'social.apps.django_app.default.models.DjangoStorage'
# SOCIAL_AUTH_EMAIL_FORM_URL = '/signup-email'
SOCIAL_AUTH_EMAIL_FORM_HTML = 'email_signup.html'
SOCIAL_AUTH_EMAIL_VALIDATION_FUNCTION = 'gestalt.web.middlewares.send_validation'
SOCIAL_AUTH_EMAIL_VALIDATION_URL = '/email/sent/'
# SOCIAL_AUTH_USERNAME_FORM_URL = '/signup-username'
SOCIAL_AUTH_USERNAME_FORM_HTML = 'username_signup.html'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
        'gestalt.web.middlewares.require_email',
    'social.pipeline.mail.mail_validation',
    'social.pipeline.user.create_user',
        'gestalt.web.middlewares.refresh_profile',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.debug.debug',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'social.pipeline.debug.debug'
)

################################################################################

AUTHENTICATION_BACKENDS = CLOUD_BACKENDS + DEFAULT_AUTH_BACKENDS

################################################################################

DEBUG          = Reactor.debug
TEMPLATE_DEBUG = DEBUG

MANAGERS = ADMINS

SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['first_name', 'last_name', 'email',
#                                         'username']

################################################################################

META_USE_OG_PROPERTIES      = True
META_USE_TWITTER_PROPERTIES = True
META_USE_GOOGLEPLUS_PROPERTIES = True
META_USE_TWITTER_PROPERTIES = True

################################################################################

CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOW_CREDENTIALS = False
CORS_REPLACE_HTTPS_REFERER = False

CORS_ORIGIN_WHITELIST = (
    META_SITE_DOMAIN,
    '*.'+META_SITE_DOMAIN,
)

CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'PUT',
    'PATCH',
    'DELETE',
    'OPTIONS'
)

CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken'
)

################################################################################

TASTYPIE_DEFAULT_FORMATS = ['json', 'xml', 'yaml', 'plist']

API_LIMIT_PER_PAGE = 0

################################################################################

DEFAULT_URL_SCHEME = META_SITE_PROTOCOL
SESSION_COOKIE_DOMAIN = '.'+META_SITE_DOMAIN

#*******************************************************************************

SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
FORCE_VARY_ON_HOST = False

#*******************************************************************************

APPEND_SLASH = False
TASTYPIE_ALLOW_MISSING_SLASH = True

TASTYPIE_FULL_DEBUG = Reactor.debug

#*******************************************************************************

AXES_VERBOSE = Reactor.debug

AXES_LOGIN_FAILURE_LIMIT = 3
AXES_LOCK_OUT_AT_FAILURE = True
AXES_USE_USER_AGENT = True
AXES_COOLOFF_TIME = None
AXES_LOGGER = 'axes.watch_login'
AXES_LOCKOUT_TEMPLATE = ''
AXES_LOCKOUT_URL = ''
AXES_USERNAME_FORM_FIELD = 'username'
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = True
AXES_NEVER_LOCKOUT_WHITELIST = True
AXES_BEHIND_REVERSE_PROXY = True
AXES_REVERSE_PROXY_HEADER = True
AXES_DISABLE_ACCESS_LOG = True

################################################################################

RQ_SHOW_ADMIN_LINK = True

RQ_QUEUES = {
    'default': {
        'URL': Reactor.varenv('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 1500,
    },
    'perso': {
        'URL': Reactor.varenv('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 5000,
    },
    'social': {
        'URL': Reactor.varenv('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 2500,
    },
    'crawler': {
        'URL': Reactor.varenv('REDIS_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 7000,
    },
    #'high': {
    #    'URL': Reactor.varenv('REDISTOGO_URL', 'redis://localhost:6379/0'), # If you're on Heroku
    #    'DEFAULT_TIMEOUT': 500,
    #},
    #'low': {
    #    'HOST': 'localhost',
    #    'PORT': 6379,
    #    'DB': 0,
    #}
    'nlp': {
        'URL': Reactor.varenv('REDISCLOUD_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 5000,
    },
    'rdf': {
        'URL': Reactor.varenv('REDISCLOUD_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 5000,
    },
    'sparql': {
        'URL': Reactor.varenv('REDISCLOUD_URL', 'redis://localhost:6379/0'), # If you're on Heroku
        'DEFAULT_TIMEOUT': 5000,
    },
}

for target,source in [
    ('perso', 'social'),
]:
    RQ_QUEUES[target] = RQ_QUEUES[source]

for key in RQ_QUEUES.keys():
    if 'DB' not in RQ_QUEUES[key]:
        RQ_QUEUES[key]['DB'] = 0

#RQ_EXCEPTION_HANDLERS = ['path.to.my.handler'] # If you need custom exception handlers

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        #"rq_console": {
        #    "format": "%(asctime)s %(message)s",
        #    "datefmt": "%H:%M:%S",
        #},
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        #"rq_console": {
        #    "level": "DEBUG",
        #    "class": "rq.utils.ColorizingStreamHandler",
        #    "formatter": "rq_console",
        #    "exclude": ["%(asctime)s"],
        #},
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        #"rq.worker": {
        #    "handlers": ["rq_console"] # , "sentry"],
        #    "level": "DEBUG"
        #},
    }
}

try:
    from uchikoma.local_settings import *
except ImportError:
    pass
